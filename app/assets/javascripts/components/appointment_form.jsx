var AppointmentForm = React.createClass({
  handleChange: function (e) {
    /*sets var name to the name of the input you are trying to change. e is the event*/
    var name = e.target.name;
    /*creates a new object to be passed back to the state*/
    obj = {};
    /*sets name to be an attribute of the object*/
    obj [name] = e.target.value;

    this.props.onUserInput(obj);
  }, 

  handleSubmit: function (e) {
    e.preventDefault();
    this.props.onFormSubmit();
  }, 
  
  render: function() {
    return (
      <div>
        <h2>Make a New Appointment</h2>
        <form onSubmit={this.handleSubmit}> 
          <input name='title' placeholder='Appointment Title' value={this.props.input_title}
            /*allows the input to be changed, and then tells what to do when it is changed*/
            onChange={this.handleChange}/>
          <input name='appt_time' placeholder='Date and Time' value={this.props.input_appt_time}
            onChange={this.handleChange}/>
          <input type='submit' value='Make Appointment' />
        </form>
      </div>
    )
  }
});