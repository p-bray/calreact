var Appointments = React.createClass({
  getInitialState: function() {
    return {
      appointments: this.props.appointments, 
      title: 'asdfa', 
      appt_time: 'asdfsadf'
    }
  },
  
  handleUserInput: function(obj) {
    /*this method sets the state of the DOM to the object passed to it from AppointmentForm.handleChange*/
    this.setState(obj);
  },
  /*this handles the AJAX submission*/
  handleFormSubmit: function() {
    /*the json object to be submitted*/
    var appointment = {title: this.state.title, appt_time: this.state.appt_time}
    

    /*the jquery ajax call to the server*/
    $.post('/appointments',
        {appointment: appointment})
      .done(function(data) {
        this.addNewAppointment(data);
      }.bind(this));
  },

  /*appends the new appointment to the appointment props*/
  addNewAppointment: function(appointment) {
    var appointments = React.addons.update(this.state.appointments, {$push: [appointment]});
    this.setState({
      appointments: appointments.sort(function(a,b){
        return new Date(a.appt_time) - new Date(b.appt_time);
    })});
  }, 

  render: function() {
    return (
      <div>
        <AppointmentForm input_title={this.state.title} input_appt_time={this.state.appt_time}
          /*so the onuserinput method will react when the user interacts. Will call the handle user input method*/
          onUserInput={this.handleUserInput} 
          onFormSubmit={this.handleFormSubmit}/> 

        <AppointmentsList appointments={this.state.appointments} />
      </div>
    )
  }
});